# Pret-a-Loger Tado


## API
- https://shkspr.mobi/blog/2019/02/tado-api-guide-updated-for-2019/

Use the following base urls for API requests:
```
Generating a token: https://auth.tado.com/oauth/token
v1 base url: https://my.tado.com/api/v1
v2 base url: https://my.tado.com/api/v2
```


### Data from the Tado
The Tado outputs the following measurements from the url ...

- Current Temperature
- Current Relative Humidity
- Current Setpoints

This data can also be retrieved from the TADO in the form of day reports. This can be retrieved from the URL `https://my.tado.com/api/v2/homes/{homeId}/zones/{zoneId}/dayReport?date={date}`
and will result in a `json` response with the data specified via `date` in `ISO8601` format. This json file has the following keys

- `callForHeat`
- `hoursInDay`
- `interval`
- `measuredData`, stores the temperature and humidity data
- `settings`, stores the historic setpoints data
- `stripes`
- `weather`
- `zoneType`



### Generating an access token and getting home ID
- For getting the client secret: visit (or curl) https://app.tado.com/env.js, field under oauth is called clientSecret
- Note that the token is valid for 600 seconds after creation

```python
# generate a token
# curl -s "https://auth.tado.com/oauth/token" -d client_id=tado-web-app -d grant_type=password -d scope=home.user -d username="you@example.com" -d password="Password123" -d client_secret=wZa
auth_url = "https://auth.tado.com/oauth/token"
data = {"client_id": "tado-web-app",
        "grant_type": "password",
        "scope": "home.user",
        "username": username,
        "password": password,
        "client_secret": tado_client_secret}

req = requests.request("POST", auth_url, data=data)
```

- Getting the homeId is easiest using the v1 URL rather than the V2 URL. The V2 returns a lot of extra (irrelevant) information

### Zones

| ID   | Name                   | type    |
| :--- | :--------------------- | :------ |
| 1    | Hall                   | HEATING |
| 2    | Kitchen/Livingroom     | HEATING |
| 3    | Bathroom               | HEATING |
| 4    | Study room             | HEATING |
| 5    | Bedroom                | HEATING |


For getting historic data from the API use:
```python
req = requests.request("POST", "https://my.tado.com/api/v2/homes/{homeId}/zones/{zoneId}/dayReport?date={date}", headers=auth_header)
```
- date should be formatted using ISO8601 format


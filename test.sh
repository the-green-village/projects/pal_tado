#!/usr/bin/bash
source ../../api-keys/kafka-cluster-info
count=15

for i in $(seq 1 $count); do
	echo "Running iteration $i/$count..."
	time python3 serializing_producer.py -p
	sleep 100
done

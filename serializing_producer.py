#!/usr/bin/python3
import requests
import json
import time
import datetime as dt
import logging

from typing import Callable

from tgvfunctions import tgvfunctions
import projectsecrets

# Setting up the logger
logging.basicConfig(
    format="%(asctime)s - %(name)s - %(levelname)s - %(message)s", level=logging.WARNING
)
logger = logging.getLogger(__file__)

# Global variables for API calls
BASE_URL_V1 = "https://my.tado.com/api/v1" # Use this for getting details
BASE_URL_V2 = "https://my.tado.com/api/v2" 
ZONES = { # Format: zone_id: ["room_name", "TUD_room_id"]
         1: ['Hall',               '25.03.00.010'], 
         2: ['Kitchen/Livingroom', '25.03.00.020'], 
         3: ['Bathroom',           '25.03.01.011'], 
         4: ['Study room',         '25.03.01.020'], 
         5: ['Bedroom',            '25.03.01.030'],}
 

def retry(func: Callable) -> Callable:
    """
    Decorator for retrying a function when it raises an exception. The main purpose of this is to elegantly retry when an API request fails.
    By default, the decorator retries 3 times with a delay of 1 second after failing before finally raising an exception

    Params
    ------
    `func: Callable` The function that needs to be retried

    Exceptions
    ----------
    `Exception`, raises an exception if the function failed after 3 tries.

    Example
    -------
    ```python
    @retry
    def foo():
        # Some function or routine that may raise an exception
    ```
    """
    def inner(*args, **kwargs):
        retries = 3
        delay = 1
        result = None
        for attempt in range(retries):
            try:
                result = func(*args, **kwargs)
                break
            except:
                if attempt >= retries:
                    raise Exception(f"Function {func.__name__} failed after {retries} attempts")
                time.sleep(delay)
        return result
    return inner


def create_message(zone_id, data) -> dict:
    project_id = "pretaloger"
    room_name, tud_room_id, serial_number = ZONES[zone_id]
    epochms = data["epochms"]
    temp = data["temp"]
    hum = data["hum"]
    set_point = data["set_point"]

    value = {
        "project_id": project_id,
        "project_description": "",
        "application_id": "Tado",
        "application_description": "",
        "device_id": f"Thermostat - {room_name}",
        "device_serial": f"{serial_number}",
        "device_description": f"Tado Zone {zone_id} - {room_name}",
        "device_manufacturer": "Tado",
        "location_id": f"Prêt-à-Loger - {room_name}",
        "location_description": f"TU Delft Room ID: {tud_room_id}",
        "timestamp": epochms,
        "measurements": [{
                "measurement_id": "Temperature",
                "unit": "°C",
                "value": temp, 
            }, {
                "measurement_id": "Temperature Setpoint",
                "unit": "°C",
                "value": set_point,
            }, {
                "measurement_id": "Relative Humidity",
                "unit": "%",
                "value": hum,
            },]}

    # device_serial = 
    # if zone_id == 2:
    #     value["device_type"] = "Smart Thermostat"

    return value


def format_time_as_api_string(time: dt.datetime) -> str:
    """
    function formats a date-time object in the format that the CloudGarden API endpoints expect
    outputted format is YYYY-MM-DD
    """
    day = str(time.day).zfill(2)
    month = str(time.month).zfill(2)
    year = str(time.year).zfill(2)
    date_formatted = f"{year}-{month}-{day}"

    return date_formatted


def str_to_epochms(date_str: str) -> int:
    seconds = dt.datetime.fromisoformat(date_str.replace("Z", "+00:00")).timestamp()
    ms = int(seconds*1000)
    return ms


@retry
def generate_token(username, password, tado_client_secret) -> str:
    # generate a token
    # curl -s "https://auth.tado.com/oauth/token" -d client_id=tado-web-app -d grant_type=password -d scope=home.user -d username="you@example.com" -d password="Password123" -d client_secret=wZa
    auth_url = "https://auth.tado.com/oauth/token"
    data = {"client_id": "tado-web-app",
            "grant_type": "password",
            "scope": "home.user",
            "username": username,
            "password": password,
            "client_secret": tado_client_secret}

    req = requests.request("POST", auth_url, data=data)
    status_code = req.status_code
    response = json.loads(req.text)

    if (status_code != 200):
        raise Exception(f"Non 200 status code: {response.status_code}, {response.text}")

    token = response["access_token"]
    return token


def create_authentication_header(username, password, tado_client_secret) -> dict:
    """
    Create an authentication header given username and password

    Return
    ------
        `auth_header: dict`, the authentication header as a python dict object
    """
    token = generate_token(username, password, tado_client_secret)
    auth_header = {"Authorization": f"Bearer {token}"}

    return auth_header


@retry
def get_home_id(auth_header: dict) -> str:
    details = requests.request("GET", f"{BASE_URL_V1}/me/", headers=auth_header)
    res = json.loads(details.text)

    if (details.status_code != 200):
        raise Exception(f"Non 200 status code: {res.status_code}, {res.text}")

    home_id = str(res["homeId"])
    return home_id


@retry
def get_zones(home_id: str, auth_header: dict) -> dict:
    end_point = f"{BASE_URL_V2}/homes/{home_id}/zones"
    res = requests.request("GET", end_point, headers=auth_header)

    if (res.status_code != 200):
        raise Exception(f"Non 200 status code: {res.status_code}, {res.text}")
    
    res = json.loads(res.text)
    # print(json.dumps(res, indent=4))
    zones = {}

    for zone in res:
        zones[zone["id"]] = {"name": zone["name"],
                             "tud_room_id": None,
                             "online": zone["devices"][0]["connectionState"]["value"],
                             "devices": zone["devices"]}
    
    return zones


@retry
def get_data_raw(home_id: str, zone_id: str, auth_header: dict) -> str:
    end_point = f"{BASE_URL_V2}/homes/{home_id}/zones/{zone_id}/state"
    res = requests.request("GET", end_point, headers=auth_header)
    data = json.loads(json.dumps(res.text))

    return data


def get_data_as_dict(home_id: str, zone_id: str, auth_header: dict) -> dict:
    data_raw = get_data_raw(home_id, zone_id, auth_header)
    data_raw = json.loads(data_raw)
    # print(json.dumps(data_raw))

    sensor_temp_data = data_raw["sensorDataPoints"]["insideTemperature"]["celsius"]
    sensor_hum_data = data_raw["sensorDataPoints"]["humidity"]["percentage"]
    sensor_data_points_time_stamp = data_raw["sensorDataPoints"]["insideTemperature"]["timestamp"]
    epochms = str_to_epochms(sensor_data_points_time_stamp)


    if zone_id == 1:
        set_point = "off"
    else:
        set_point = data_raw["setting"]["temperature"]["celsius"]

    data_dict = {
            "time": sensor_data_points_time_stamp,
            "epochms": epochms,
            "temp": sensor_temp_data,
            "hum": sensor_hum_data,
            "set_point": set_point,
            }

    return data_dict

def main():
    username = projectsecrets.PAL_TADO_API_USERNAME
    password = projectsecrets.PAL_TADO_API_PASSWORD
    tado_client_secret = projectsecrets.PAL_TADO_CLIENT_SECRET
    topic = projectsecrets.TOPIC
    producer_name = "pal_tado"

    tgv = tgvfunctions.TGVFunctions(topic)
    producer = tgv.make_producer(producer_name)

    # Create auth header and retrieve zones
    auth_header = create_authentication_header(username, password, tado_client_secret)
    home_id = get_home_id(auth_header)
    zones = get_zones(home_id, auth_header)

    for zone_id, zone_data in zones.items():
        for device in zone_data["devices"]:
            if "ZONE_LEADER" in device["duties"]:
                measurement_device_serial = device["serialNo"]
                ZONES[zone_id].append(measurement_device_serial)

    for zone_id, zone_data in zones.items():
        if zone_data["online"] != True:
            continue
        data = get_data_as_dict(home_id, zone_id, auth_header)
        value = create_message(zone_id, data)
        tgv.produce(producer, value)


if __name__ == "__main__":
    main()

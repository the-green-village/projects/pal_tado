#!/usr/bin/python3
import requests
import json
import datetime as dt
import pandas as pd
import projectsecrets
from tgvfunctions import tgvfunctions
from serializing_producer import *


def format_df_row_as_dict(df_row: pd.DataFrame) -> dict:
    data_dict = {
            "time": df_row["timestamp"],
            "epochms": df_row["epochms"],
            "temp": df_row["temperature"],
            "hum": df_row["humidity"],
            "set_point": df_row["set_point"],
            }
    return data_dict


def get_day_report(date: str, home_id: int, zone_id: int, auth_header: dict) -> pd.DataFrame | None:
    """
    Retrieve all measurements from a specified date. Returns a dataframe with the following
    columns:

        ["timestamp", "epochms", "temperature", "humidity", "set_point"])

    Params
    ------
        `date: dt.datetime`, the date for which to retrieve a date, formatted as a datetime object
        `home_id: int`, the home_id corresponding to the Tado to use for retrieving data
        `zone_id: int`, the zone id corresponding to the specific zone in the tado to use for retrieving the data
        `auth_header: dict`, the authentication header used for the API request

    Return
    ------
        `df: pd.DataFrame`, the dataframe containing the data from the measurements in the day report
    """
    end_point = f"{BASE_URL_V2}/homes/{home_id}/zones/{zone_id}/dayReport?date={date}"
    res = requests.request("GET", end_point, headers=auth_header)

    if zone_id == 1:
        return None

    if res.status_code != 200:
        print(f"Non 200 status code: {res.status_code}")
        return None

    res = json.loads(res.text)



    # print(json.dumps(res, indent=4))

    rows = []
    temp_data_points = res["measuredData"]["insideTemperature"]["dataPoints"]
    hum_data_points = res["measuredData"]["humidity"]["dataPoints"]
    set_points = res["settings"]["dataIntervals"]

    for i in range(len(temp_data_points)):
        timestamp = dt.datetime.fromisoformat(temp_data_points[i]["timestamp"].replace("Z", "+00:00"))
        epochms = str_to_epochms(temp_data_points[i]["timestamp"])
        temp = temp_data_points[i]["value"]["celsius"]
        hum = hum_data_points[i]["value"]
        set_point = None

        for sp in set_points:
            time_from = str_to_epochms(sp["from"])
            time_to = str_to_epochms(sp["to"])
            if time_from <= epochms <= time_to:
                set_point = sp["value"]["temperature"]["celsius"]

        data_row = [timestamp, epochms, temp, hum, set_point]
        rows.append(data_row
                    )
    df = pd.DataFrame(rows, columns=["timestamp", "epochms", "temperature", "humidity", "set_point"])
    return df


def main():
    username = projectsecrets.PAL_TADO_API_USERNAME
    password = projectsecrets.PAL_TADO_API_PASSWORD
    tado_client_secret = projectsecrets.PAL_TADO_CLIENT_SECRET
    topic = projectsecrets.TOPIC
    producer_name = "pal_tado_producer"

    tgv = tgvfunctions.TGVFunctions(topic)

    # Create authentication header
    auth_header = create_authentication_header(username, password, tado_client_secret)
    home_id = get_home_id(auth_header)
    zones = get_zones(home_id, auth_header)

    for zone_id in zones:
        for device in zones[zone_id]["devices"]:
            if "ZONE_LEADER" in device["duties"]:
                measurement_device_serial = device["serialNo"]
                ZONES[zone_id].append(measurement_device_serial)

    # Set start and end dates for retrieving historic data
    date = dt.datetime(year=2023, month=5, day=9)
    final_date = dt.datetime(year=2024, month=4, day=3)

    producer = tgv.make_producer(producer_name)
    previous_last_time_stamp = {
            1: None,
            2: None,
            3: None,
            4: None,
            5: None,
            }

    while date < final_date:
        auth_header = create_authentication_header(username, password, tado_client_secret)
        for zone_id in zones.keys():
            date_str = format_time_as_api_string(date)
            data = get_day_report(date_str, home_id, zone_id, auth_header)
            print(data)
            if data is None:
                print("Nothing found, moving on")
                continue
            else:
                # Drop all the data points that were also included in the previous day report
                first_time_stamp = data.timestamp.iloc[0]
                if (previous_last_time_stamp[zone_id] is not None 
                    and previous_last_time_stamp[zone_id] > first_time_stamp):
                    data = data[data["timestamp"] > previous_last_time_stamp[zone_id]]

                last_time_stamp = data.timestamp.iloc[-1]
                for _, row in data.iterrows():
                    data_dict = format_df_row_as_dict(row)
                    msg = create_message(zone_id, data_dict)
                    tgv.produce(producer, msg)

                # Update the previous last time stamp
                previous_last_time_stamp[zone_id] = last_time_stamp
                    
        date += dt.timedelta(days=1)
        print(f"Next date: {date}")

if __name__ == "__main__":
    main()
